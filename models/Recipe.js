var mongoose = require('mongoose');

const route = 'recipe'; 	
const modelId = 'Recipe';  	

var Schema = new mongoose.Schema({


	
	name: {type: String, required: true},
	author: String,
	type: String,
	feeds: Number,
	updated_at: { type: Date, default: Date.now },

	

	owner: {
		type: String,
		required: true
	}
});

 

module.exports = {
	model: mongoose.model(modelId, Schema),
	route: route
} 